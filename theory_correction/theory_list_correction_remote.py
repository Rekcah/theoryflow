from openai import OpenAI
import configparser

config = configparser.ConfigParser()
config.read("configurations/config.ini")

if "API" not in config or "api_key" not in config["API"]:
    raise ValueError("API key non trovata nel file config.ini")

api_key = config["API"]["api_key"]
client = OpenAI(
    base_url="https://integrate.api.nvidia.com/v1",
    api_key=api_key
)
def correggi_grammatica_nvidia(testo):
    completion = client.chat.completions.create(
        model="igenius/colosseum_355b_instruct_16k",
        messages=[{"role": "user", "content": f"Restituisci esclusivamente la frase grammaticalmente corretta, in italiano, senza alterarne il significato, senza aggiungere spiegazioni e senza inserire nulla tra parentesi, risposta concisa e diretta: {testo}"}],
        temperature=0.2,
        top_p=0.7,
        max_tokens=1024,
        stream=False # if false the response will be returned complete
    )
    response_content = completion.choices[0].message.content
    return response_content.strip()

with open("test/frasi_di_training.txt", "r", encoding="utf-8") as file:
    frasi = file.readlines()

with open("test/frasi_corrette.txt", "w", encoding="utf-8") as output_file:
    for frase in frasi:
        frase = frase.strip()
        if frase:
            try:
                frase_corretta = correggi_grammatica_nvidia(frase)
                output_file.write(f"Originale: {frase}\n")
                output_file.write(f"Corretta: {frase_corretta}\n\n")
            except Exception as e:
                output_file.write(f"Errore con la frase: {frase}\nDettagli: {str(e)}\n\n")

print("Correzione completata. Le frasi corrette sono state salvate in 'frasi_corrette.txt'.")
