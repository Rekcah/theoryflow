from transformers import AutoTokenizer, AutoModelForCausalLM
import torch

model_name = "iGeniusAI/Italia-9B-Instruct-v0.1"
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name, device_map="auto")

def correggi_grammatica(testo):
    """Corregge la grammatica di una frase utilizzando iGeniusAI."""
    prompt = f"Restituisci esclusivamente la frase grammaticalmente corretta, in italiano, senza alterarne il significato, senza aggiungere spiegazioni e senza inserire nulla tra parentesi, risposta concisa e diretta: {testo}"
    inputs = tokenizer(prompt, return_tensors="pt").to(model.device)
    outputs = model.generate(
        **inputs,
        max_new_tokens=100,
        temperature=0.7,
        do_sample=False
    )
    result = tokenizer.decode(outputs[0], skip_special_tokens=True)
    return result.strip()

with open("test/frasi_di_training.txt", "r", encoding="utf-8") as file:
    frasi = file.readlines()

with open("test/frasi_corrette.txt", "w", encoding="utf-8") as output_file:
    for frase in frasi:
        frase = frase.strip()
        if frase:
            frase_corretta = correggi_grammatica(frase)
            output_file.write(f"Originale: {frase}\n")
            output_file.write(f"Corretta: {frase_corretta}\n\n")

print("Correzione completata. Le frasi corrette sono state salvate in 'frasi_corrette.txt'.")
