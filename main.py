from fastapi import FastAPI, Query
import configparser
from theory_correction.theory_correction_remote import suggest_correction
from request_or_input import question_or_affirmation
from test import test_query


app = FastAPI()
config = configparser.ConfigParser()
config.read('configurations/translations.ini')

@app.get("/")
def get_documentation(lang: str = Query("IT", enum=[section for section in config.sections()])):
    return config[lang]['documentation']

@app.post("/suggest_correction")
def suggest_correction_endpoint(text: str):
    correction = suggest_correction(text)
    return {"suggested": correction}

@app.post("/request_or_affirmation")
def type_of_phrase(text: str):
    type = question_or_affirmation(text)
    #if type[-1] == "?":
    #   TODO inserire un controllo per verificare se la teoria è già presente nella banca dati
    #   TODO se già presente restituire un messaggio indicando che si potrebbe trattare di una frase retorica provocatoria
    #   TODO altrimenti considera la frase come richiesta
    #else:
    return {"type": type}

@app.post("/test")
def test(text: str):
    type = test_query(text)
    return {"response": type}