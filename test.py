from openai import OpenAI
import configparser

config = configparser.ConfigParser()
config.read("configurations/config.ini")

if "API" not in config or "api_key" not in config["API"]:
    raise ValueError("API key non trovata nel file config.ini")

api_key = config["API"]["api_key"]
client = OpenAI(
    base_url="https://integrate.api.nvidia.com/v1",
    api_key=api_key
)

def test_query(testo: str) -> str:
    completion = client.chat.completions.create(
        model="igenius/colosseum_355b_instruct_16k",
        messages=[{"role": "user", "content": f"{testo}"}],
        temperature=0.2,
        top_p=0.7,
        max_tokens=1024,
        stream=False
    )
    response_content = completion.choices[0].message.content
    return response_content.strip()
