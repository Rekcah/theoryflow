import spacy

# Carica il modello della lingua italiana di spacy
nlp = spacy.load("it_core_news_sm")

def extract_atomic_propositions(text):
    doc = nlp(text)
    atomic_propositions = []

    for sent in doc.sents:
        for token in sent:
            if token.dep_ == 'cc':  # Coordinating conjunction (e, ma, etc.)
                part1 = sent[:token.i].text
                part2 = sent[token.i + 1:].text
                if part1:
                    atomic_propositions.append(part1)
                if part2:
                    atomic_propositions.append(part2)
                break
        else:
            atomic_propositions.append(sent.text)

    return atomic_propositions

def main():
    input_sentence = input("Inserisci una frase: ")
    atomic_propositions = extract_atomic_propositions(input_sentence)

    print("Proposizioni atomiche trovate:")
    for proposition in atomic_propositions:
        print(proposition)

if __name__ == "__main__":
    main()

