import spacy

# Carica il modello di lingua italiana di spaCy
nlp = spacy.load("it_core_news_lg")

#Definisci la lista degli elementi ontologicamente trascurabili
elementi_ontologicamente_trascurabili = ["è","."]

# Funzione per trovare il soggetto in una frase
def trova_soggetto(frase):
    doc = nlp(frase)
    soggetto = None

    for token in doc:
        if "subj" in token.dep_:
            soggetto = token.text
            break

    return soggetto

# Funzione per elaborare una frase e creare la concatenazione con "⊇" e i tipi di parole accanto alle parole
def elabora_frase(frase):
    doc = nlp(frase)

    # Rimuovi la parola "è" dalla frase e crea la lista delle parole con tipi
    parole_e_tipi = [f"{token.text} ({token.pos_})" for token in doc if token.text.lower() not in elementi_ontologicamente_trascurabili]

    # Stampa i tipi di parole accanto alle parole
    for parola_tipo in parole_e_tipi:
        print(parola_tipo)

    # Concatena le parole con "⊇"
    concatenazione = "⊇".join(token.text for token in doc if token.text.lower() not in elementi_ontologicamente_trascurabili)

    # Racchiudi la concatenazione tra "∃{" e "}"
    frase_racchiusa = f"∃{{{concatenazione}}}"

    return frase_racchiusa

# Frase di esempiofrase_input = "Il tavolo è un mobile formato da un piano sostenuto da gambe"
frase_input = "la velocità della luce nel vuoto è costante per tutti gli osservatori indipendentemente dal loro stato di movimento"

# Elabora la frase di esempio
risultato = elabora_frase(frase_input)

# Stampa il risultato senza i tipi nell'ultima riga di output
print()
print(risultato,"\n")

# Trova il soggetto nella frase di esempio
soggetto = trova_soggetto(frase_input)
if soggetto:
    print(f"Il riferimento della frase è: {soggetto}")
else:
    print("Nessun soggetto trovato nella frase.")
