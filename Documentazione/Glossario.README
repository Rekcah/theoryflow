POS (Part-of-Speech) tagging:
    ADJ: Aggettivo
    ADP: Adposizione/Preposizione
    ADV: Avverbio
    AUX: Verbo ausiliario
    CONJ: Congiunzione
    CCONJ: Congiunzione di coordinazione
    DET: Determinante
    INTJ: Interiezione
    NOUN: Sostantivo
    NUM: Numero
    PART: Particella
    PRON: Pronome
    PROPN: Nome proprio
    PUNCT: Punteggiatura
    SCONJ: Congiunzione subordinativa
    SYM: Simbolo
    VERB: Verbo
    X: Altro

Analisi delle dipendenze:
    ROOT: Radice della frase
    acl: Modificatore dell'aggettivo/clause (adjectival clause modifier)
    acomp: Modificatore dell'aggettivo (adjectival complement)
    advcl: Clause avverbiale (adverbial clause modifier)
    advmod: Modificatore dell'avverbio (adverbial modifier)
    agent: Agente (agent)
    amod: Modificatore dell'aggettivo (adjectival modifier)
    appos: Apposizione (appositional modifier)
    attr: Attributo (attribute)
    aux: Verbo ausiliario (auxiliary)
    auxpass: Verbo ausiliario passivo (passive auxiliary)
    case: Caso (case marking)
    cc: Congiunzione coordinativa (coordinating conjunction)
    ccomp: Complemento del verbo (clausal complement)
    compound: Parola composta (compound)
    conj: Congiunzione coordinativa (conjunct)
    cop: Verbo copula (copula)
    csubj: Soggetto clausola (clausal subject)
    csubjpass: Soggetto clausola passiva (clausal passive subject)
    dative: Complemento di dativo (dative)
    dep: Dipendenza strutturale (unclassified dependent)
    det: Determinante (determiner)
    dobj: Oggetto diretto (direct object)
    expl: Pronome esplorativo (expletive)
    intj: Interiezione (interjection)
    mark: Introduzione complemento subordinato (marker introducing a complement clause)
    meta: Elemento meta (meta modifier)
    neg: Negazione (negation modifier)
    nmod: Modificatore nominale (nominal modifier)
    npmod: Modificatore di frase nominale (noun phrase as adverbial modifier)
    nsubj: Soggetto nominale (nominal subject)
    nsubjpass: Soggetto nominale passivo (nominal passive subject)
    nummod: Modificatore numerale (numeric modifier)
    oprd: Complemento oggetto (object predicate)
    parataxis: Elemento in paratassi (parataxis)
    punct: Punteggiatura (punctuation)
    quantmod: Modificatore quantificatore (quantifier modifier)
    relcl: Clausola relativa (relative clause modifier)
    xcomp: Complemento del verbo non finito (open clausal complement)


GERARCHIA ontologica:
1. Quantificatore esistenziale.
2. DET(indeterminativo).
3. NOUN, PRON, PRON.
4. ADJ.
VERB, AUX.
ADV.
CONJ.
SCONJ.
PUNCT.

Negazione.
Congiunzione.
Disgiunzione.
Esclusione.
Implicazione.
Doppia implicazione.

IGNORABILI:
1. DET (determinativi e pronomi dimostrativi).
2. INTJ.
3. 

AMBIGUI:
1. SYM.
2. 



Questioni:

- VERB: quando viene specificato l'oggetto si attua un AND tra le due proprietà relative al soggetto:
    Maria ama il suo gatto = Maria ama AND Maria suo gatto

- DET:
    ["Il","Lo"]:
        → articolo determinativo:
            → di → genere maschile
            → singolare
    ["La"]:
        → articolo → determinativo → di → genere → femminile
        → singolare
    ["L']:
        → articolo → determinativo → di → genere → neutrale
        → singolare
    ["I","Gli"]:
        → articolo → determinativo → di → genere → neutrale
        → plurale
    ["Le"]:
        → articolo → determinativo → di → genere → femminile
        → plurale
    ["Un","Uno"]:
        → articolo → indeterminativo → di → genere → maschile
        → singolare
        → un → numero
    ["Una","Un'"]:
        → articolo → indeterminativo → di → genere → femminile
        → singolare
    ["Dello", "Del"]:
        → articolo → determinativo → di → genere → maschile
        → singolare
        → insieme → semantico → delle → parole → di → e → l'
    ["Della"]:
        → articolo → determinativo → di → genere → femminile
        → singolare
        → insieme → semantico → delle → parole → di → e → la
    ["Dell'"]:
        → articolo → determinativo → di → genere → neutrale
        → singolare
        → insieme → semantico → delle → parole → di → e → l'
    ["Dei"]:
        → articolo → determinativo → di → genere → neutrale
        → plurale
        → insieme → semantico → delle → parole → di → e → i
    ["Degli"]:
        → articolo → determinativo → di → genere → neutrale
        → plurale
        → insieme → semantico → delle → parole → di → e → gli
    ["Delle"]:
        → articolo determinativo → di → genere → maschile
        → plurale
        → insieme → semantico → delle → parole → di → e → le
    ["Questo"]:
        → pronome → dimostrativo → di → genere → maschile
        → singolare
        → ecco-istum → ecco-cotesto → ecco id istud → ecco qui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Questa"]:
        → pronome → dimostrativo → di → genere → femminile
        → singolare
        → ecco-istum → ecco-cotesto → ecco id istud → ecco qui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Questi"]:
        → pronome → dimostrativo → di → genere → neutrale
        → plurale
        → ecco-istum → ecco-cotesto → ecco id istud → ecco qui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Queste"]:
        → pronome → dimostrativo → di → genere → femminile
        → plurale
        → ecco-istum → ecco-cotesto → ecco id istud → ecco qui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Quello"]:
        → pronome → dimostrativo → di → genere → maschile
        → singolare
        → aquelle → ecco illum → ecco lui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Quella"]:
        → pronome → dimostrativo → di → genere → femminile
        → singolare
        → aquelle → ecco illum → ecco lei → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Quelli"]:
        → pronome → dimostrativo → di → genere → neutrale
        → plurale
        → aquelle → ecco illum → ecco lui → esplicitazione della proprietà dinnanzi al riferimento narrativo.
    ["Quelle"]:
        → pronome → dimostrativo → di → genere → femminile
        → plurale
        →
    ["Mio"]:
        →
    ["Mia"]:
        →
    ["Miei"]:
        →
    ["Mie"]:
        →
    ["Tuo"]:
        →
    ["Tua"]:
        →
    ["Tuoi"]:
        →
    ["Tue"]:
        →
    ["Suo"]:
        →
    ["Sua"]:
        →
    ["Suoi"]:
        →
    ["Sue"]:
        →
    ["Nostro"]:
        →
    ["Nostra"]:
        →
    ["Nostri"]:
        →
    ["Nostre"]:
        →
    ["Vostro"]:
        →
    ["Vostra"]:
        →
    ["Vostri"]:
        →
    ["Vostre"]:
        →
    ["Loro"]:
        →

Proprietà fondamentali non contraddittorie:
1. Avere almeno la proprietà di essere una proprietà → qualitativa.
2. Avere la proprietà di essere una proprietà → quantitativa.
3. Avere la proprietà di avere almeno una proprietà → dialettica.
4. Avere la coerenza tra le proprietà.
5. Rispettare tutti e 5 i punti.


Nel caso dei verbi, se presente una proposizione logica
successiva al DET, non seguito tuttavia da un altro verbo, 

Maria mangia la pizza e le patatine
Maria⊇mangia⊇la⊇pizza AND Maria⊇mangia⊇le⊇patatine

Maria corre e Alberto mangia
Maria⊇corre AND Alberto⊇corre

COSE DA IMPLEMENTARE:
- soggetto sottinteso (vedi ellisse per formulazione logica)


